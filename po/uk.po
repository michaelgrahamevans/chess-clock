# Ukrainian translation for chess-clock.
# Copyright (C) 2023 chess-clock's COPYRIGHT HOLDER
# This file is distributed under the same license as the chess-clock package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: chess-clock main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/chess-clock/issues\n"
"POT-Creation-Date: 2023-09-27 23:07+0000\n"
"PO-Revision-Date: 2023-09-28 09:16+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: data/com.clarahobbs.chessclock.desktop.in:3
#: data/com.clarahobbs.chessclock.appdata.xml.in:6 src/gtk/window.ui:6
msgid "Chess Clock"
msgstr "Шаховий годинник"

#: data/com.clarahobbs.chessclock.desktop.in:9
msgid "chess;game;clock;timer;"
msgstr "chess;game;clock;timer;шахи;гра;годинник;таймер;"

#: data/com.clarahobbs.chessclock.appdata.xml.in:7
msgid "Time games of over-the-board chess"
msgstr "Відлік часу для ігор у шахи на звичайній дошці"

#: data/com.clarahobbs.chessclock.appdata.xml.in:9
msgid ""
"Chess Clock is a simple application to provide time control for over-the-"
"board chess games. Intended for mobile use, players select the time control "
"settings desired for their game, then the black player taps their clock to "
"start white's timer. After each player's turn, they tap the clock to start "
"their opponent's, until the game is finished or one of the clocks reaches "
"zero."
msgstr ""
"«Шаховий годинник» є простою програмою, яка надає засоби керування часом для "
"ігор на звичайній шаховій дошці. Програму призначено для використання на "
"мобільних пристроях. Гравці вибирають бажані для гри параметри керування "
"часом, потім гравець чорними запускає годинник для гравця білими. Після "
"кожного ходу гравець запускає таймер для свого суперника, аж доки гру не "
"буде завершено або відлік за годинником завершиться нульовим значенням."

#: data/com.clarahobbs.chessclock.appdata.xml.in:18
msgid ""
"The main screen, showing timers for the white and black players of a chess "
"game"
msgstr ""
"Головне вікно, у якому показано таймери для гравців у шахи білими і чорними"

#: data/com.clarahobbs.chessclock.appdata.xml.in:22
msgid "The time control selection screen"
msgstr "Сторінка вибору керування часом"

#: data/com.clarahobbs.chessclock.appdata.xml.in:26
msgid ""
"The main screen in a portrait aspect ratio, showing one timer rotated 180 "
"degrees"
msgstr ""
"Головне вікно у книжковому режимі, показано один таймер, який обернуто на 180"
" градусів"

#: data/com.clarahobbs.chessclock.appdata.xml.in:33
msgid "Clara Hobbs"
msgstr "Clara Hobbs"

#: src/gtk/timecontrolentry.ui:23
msgid "One minute per side, plus zero seconds per turn"
msgstr "Одна хвилина на гравця, плюс нуль секунд на хід"

#: src/gtk/timecontrolentry.ui:36
msgid "Two minutes per side, plus one second per turn"
msgstr "Дві хвилини на гравця, плюс одна секунда на хід"

#: src/gtk/timecontrolentry.ui:49
msgid "Five minutes per side, plus zero seconds per turn"
msgstr "П'ять хвилин на гравця, плюс нуль секунд на хід"

#: src/gtk/timecontrolentry.ui:62
msgid "Five minutes per side, plus three seconds per turn"
msgstr "П'ять хвилин на гравця, плюс три секунди на хід"

#: src/gtk/timecontrolentry.ui:75
msgid "Ten minutes per side, plus zero seconds per turn"
msgstr "Десять хвилин на гравця, плюс нуль секунд на хід"

#: src/gtk/timecontrolentry.ui:88
msgid "Ten minutes per side, plus five seconds per turn"
msgstr "Десять хвилин на гравця, плюс п'ять секунд на хід"

#: src/gtk/timecontrolentry.ui:101
msgid "Thirty minutes per side, plus zero seconds per turn"
msgstr "Тридцять хвилин на гравця, плюс нуль секунд на хід"

#: src/gtk/timecontrolentry.ui:114
msgid "Thirty minutes per side, plus twenty seconds per turn"
msgstr "Тридцять хвилин на гравця, плюс двадцять секунд на хід"

#: src/gtk/timecontrolentry.ui:134
msgid "Main time minutes"
msgstr "Хвилини основного часу"

#: src/gtk/timecontrolentry.ui:153
msgid "Main time seconds"
msgstr "Секунди основного часу"

#: src/gtk/timecontrolentry.ui:171
msgid "Increment seconds"
msgstr "Приріст секунд"

#: src/gtk/help-overlay.ui:11
msgctxt "shortcut window"
msgid "Game"
msgstr "Гра"

#: src/gtk/help-overlay.ui:14
msgctxt "shortcut window"
msgid "New Time Control"
msgstr "Новий відлік часу"

#: src/gtk/help-overlay.ui:20
msgctxt "shortcut window"
msgid "Restart Timer"
msgstr "Перезапустити таймер"

#: src/gtk/help-overlay.ui:28
msgctxt "shortcut window"
msgid "General"
msgstr "Загальне"

#: src/gtk/help-overlay.ui:31
msgctxt "shortcut window"
msgid "Open Main Menu"
msgstr "Відкрити головне меню"

#: src/gtk/help-overlay.ui:37
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Показати клавіатурні скорочення"

#: src/gtk/help-overlay.ui:43
msgctxt "shortcut window"
msgid "Close Window"
msgstr "Закрити вікно"

#: src/gtk/help-overlay.ui:49
msgctxt "shortcut window"
msgid "Quit"
msgstr "Вийти"

#: src/gtk/window.ui:17
msgctxt "a11y"
msgid "Select time controls"
msgstr "Вибрати відлік часу"

#: src/gtk/window.ui:44
msgid "Welcome to Chess Clock"
msgstr "Вітаємо у «Шаховому годиннику»"

#: src/gtk/window.ui:54
msgid "Select time controls below to get started"
msgstr "Спочатку виберіть параметри відліку"

#: src/gtk/window.ui:72
msgid "Time control method"
msgstr "Спосіб керування часом"

#: src/gtk/window.ui:76
msgid "Increment"
msgstr "Збільшення"

#: src/gtk/window.ui:77
msgid "Bronstein delay"
msgstr "Затримка Бронштейна"

#: src/gtk/window.ui:78
msgid "Simple delay"
msgstr "Проста затримка"

#: src/gtk/window.ui:90
msgid "Start Game"
msgstr "Почати гру"

#: src/gtk/window.ui:180
msgid "Main Menu"
msgstr "Головне меню"

#: src/gtk/window.ui:197
msgid "Toggle Pause"
msgstr "Увімкнути/Вимкнути паузу"

#: src/gtk/window.ui:214
msgctxt "a11y"
msgid "Player white timer"
msgstr "Таймер гравця білими"

#: src/gtk/window.ui:225
msgctxt "a11y"
msgid "Player black timer"
msgstr "Таймер гравця чорними"

#: src/gtk/window.ui:258
msgid "_New Time Control"
msgstr "С_творити керування часом"

#: src/gtk/window.ui:262
msgid "_Restart Timer"
msgstr "П_ерезапустити таймер"

#: src/gtk/window.ui:268
msgid "_Mute Alert Sound"
msgstr "_Вимкнути звук попередження"

#: src/gtk/window.ui:274
msgid "_Keyboard Shortcuts"
msgstr "_Клавіатурні скорочення"

#: src/gtk/window.ui:278
msgid "_About Chess Clock"
msgstr "_Про «Шаховий годинник»"

#: src/main.py:60
msgid "translator-credits"
msgstr "Юрій Чорноіван <yurchor@ukr.net>, 2023"

#: src/timerbutton.py:56
msgid "Player white timer"
msgstr "Таймер гравця білими"

#: src/timerbutton.py:61
msgid "Player black timer"
msgstr "Таймер гравця чорними"

#~ msgctxt "a11y"
#~ msgid "Playing"
#~ msgstr "Гра"

#~ msgid "Custom"
#~ msgstr "Нетиповий"

#~ msgctxt "shortcut window"
#~ msgid "Open Primary Menu"
#~ msgstr "Відкрити головне меню"

#~ msgid "Three minutes per side, plus zero seconds per turn"
#~ msgstr "Три хвилини на гравця, плюс нуль секунд на хід"

#~ msgid "Three minutes per side, plus two seconds per turn"
#~ msgstr "Три хвилини на гравця, плюс дві секунди на хід"

#~ msgid "Fifteen minutes per side, plus ten seconds per turn"
#~ msgstr "П'ятнадцять хвилин на гравця, плюс десять секунд на хід"

#~| msgctxt "shortcut window"
#~| msgid "Open Primary Menu"
#~ msgid "Primary Menu"
#~ msgstr "Основне меню"
